# biodivMapR v1.0.2

## Fixes

## Changes
- moved examples to repository root: example files are not installed with package anymore.
- removed Plots reprojection from `diversity_from_plots`: 
now, Plots and Raster must be in the same projection. Changed example Plots projection accordingly.
- removed `get_projection` (useless)

# biodivMapR v1.0.1 (Release date: 2019-09-27)

- Added NEWS.md
- Updated README.md: transfered from gitlab.irstea to github
- changed return() into return(invisible())
- updated vignettes & tutorial with latest outputs and figures from code

# biodivMapR v1.0.0 (Release date: 2019-09-26)

First release in GitHub
Submission accepted to Methods in Ecology and Evolution
